﻿using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Linq;

namespace tmc.JPEG_EXIF_Tool
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> logger = new Dictionary<string, string>();
            var dirI = new DirectoryInfo(args[0]);
            try
            {
                JPegFlipper(dirI, logger);
            }
            catch(Exception e)
            {
                logger.Add("ERROR", e.ToString());
            }
            finally
            {
                //log at any rate...we must see where the program crashed...eg out of memory
                using (StreamWriter sw = new StreamWriter(Path.Combine(dirI.FullName, "LogJPG.txt")))
                {
                    StringBuilder sb = new StringBuilder();
                    logger.ToList().ForEach(s =>
                    {
                        sb.AppendLine(s.Key + "------- " + s.Value);

                    });
                    sw.WriteLine(sb.ToString());
                    sw.Flush();
                    sw.Close();
                }
            }
            
            
           




            //var dirI = new DirectoryInfo(args[0]);
            //Dictionary<string, (int, int, int, int)> logger = new Dictionary<string, (int, int, int, int)>();
            //ControlForLogsAndNumber(dirI, logger);
            //using (StreamWriter sw = new StreamWriter(Path.Combine(dirI.FullName, "Log.txt")))
            //{
            //    StringBuilder sbError = new StringBuilder();
            //    StringBuilder sbOK = new StringBuilder();
            //    logger.ToList().ForEach(s =>
            //    {
            //        if ((s.Value.Item1 != s.Value.Item2) || (s.Value.Item3 > 0) || (s.Value.Item4 > 0))

            //            sbError.AppendLine(s.Key + "------- " + "RGB1-jpgs: " + s.Value.Item1 + " " +
            //                                        "RGB2-jpgs: " + s.Value.Item2 + " " + "RGB1-logs: " + s.Value.Item3 + " " + "RGB2-logs: " + s.Value.Item4);

            //        else
            //            sbOK.AppendLine(s.Key + "------- " + "OK");

            //    } );


            //    sw.WriteLine(sbError.ToString());
            //    sw.WriteLine("****************************");
            //    sw.WriteLine(sbOK.ToString());
            //    sw.Flush();
            //    sw.Close();

            //}



        }

        public static void ControlForLogsAndNumber(DirectoryInfo dirI, Dictionary<string, (int, int, int , int)> logger)
        {
            List<DirectoryInfo> dirsAll = dirI.EnumerateDirectories().ToList();
            //only Folders named RGB_1 or RGB_2 (or 105_RGB_1 ... or  ???) contain jpgs resp logs....
            List<DirectoryInfo> dirsRGB = dirI.EnumerateDirectories("RGB_*").
                Where(s => (s.Name.Contains("RGB_1")) || (s.Name.Contains("RGB_2"))).ToList<DirectoryInfo>();
            if (dirsRGB.Count == 2)
            {
                DirectoryInfo dirInfoRGB1 = dirsRGB.Find(s => s.Name.Contains("RGB_1"));
                DirectoryInfo dirInfoRGB2 = dirsRGB.Find(s => s.Name.Contains("RGB_2"));
                int rgb1Jpg = dirInfoRGB1.GetFiles("*.jpg").Length;
                int rgb2Jpg = dirInfoRGB2.GetFiles("*.jpg").Length;
                int rgb1Log = dirInfoRGB1.GetFiles("*.log").Length;
                int rgb2Log = dirInfoRGB2.GetFiles("*.log").Length;
                logger.Add(dirI.FullName, (rgb1Jpg, rgb2Jpg, rgb1Log, rgb2Log));
            }
            else if (dirsRGB.Count > 2)
            {

            }
            else if (dirsRGB.Count == 1)
            {

            }
            else
            {
                //we don't have to remove items because we are in the case 0
                //dirsAll.RemoveAll(dI => dirsRGB.Contains(dI));
                logger.Add(dirI.FullName, (0,0,0,0));
                foreach(var dirInfo in dirsAll)
                {
                    ControlForLogsAndNumber(dirInfo, logger);
                }
            }
            
           
        }


        public static void JPegFlipper(DirectoryInfo dirI, Dictionary<string, string> logger)
        {

            Console.WriteLine(string.Empty);
            Console.WriteLine(dirI.FullName);
            logger.Add(dirI.FullName, "DIR");
            int countJPG = 0;
            int countErr = 0;
            foreach (var fi in dirI.GetFiles("*.jpg"))
            {
                try
                {
                    using (Image jpg = Image.FromFile(fi.FullName))
                    {
                                              
                        jpg.RotateFlip(RotateFlipType.Rotate180FlipNone);
                        jpg.Save(fi.FullName, ImageFormat.Jpeg);
                        countJPG++;
                                                

                    }

                }
                catch (Exception e)
                {
                    logger.Add(fi.FullName, e.ToString());
                    countErr++;
                }
            }
            if(countErr + countJPG > 0)
            {
                logger.Add(dirI.FullName + "****PROCESSED" , "FLIPPED : " + countJPG.ToString() + " ERRORs : " + countErr.ToString());
            }
            foreach (var sub in dirI.GetDirectories())
            {
               JPegFlipper(sub, logger);
            }
           

        }

    }
}
