﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tmc.BatchProcessing
{
    public class CommandLine
    {
        private readonly List<string> cmdLineArgs;
        public CommandLine(string taskName, List<string> _cmdLineArgs)
        {
            if(string.IsNullOrWhiteSpace(taskName))
            {
                throw new ArgumentException($"Name of {nameof(taskName)} must not be in empty in class {nameof(CommandLine)}");
            }
            TaskName = taskName;
            this.cmdLineArgs = new List<string>();
            foreach (var cmd in _cmdLineArgs)
            {
                AddArg(cmd);
            }
        }

        public string TaskName { get; set; }

        public List<string> CmdLineArgs { get => cmdLineArgs; }

        public void AddArg(string cmd)
        {
            cmdLineArgs.Add(cmd);
        }
    }
}
