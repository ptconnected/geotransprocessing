﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Microsoft.Office.Core;
using Excel = Microsoft.Office.Interop.Excel;

namespace Tmc.BatchProcessing
{
    public struct CellContentByColumn
    {
        public CellContentByColumn(int column, string content)
        {
            Column = column;
            Content = content;
        }

        public int Column { get; set; }
        public string Content { get; set; }

    }

    public class ExcelAccess
    {

        public const string xlsName= "Auswertetabelle.xls";
        public const string sheetCmdLine = "FilesAndCmds";
        public const string sheetPTS_2_DAT = "PTS_2_DAT";
        public const string sheetDAT_2_PTS = "DAT_2_PTS";
        public const string sheetCoordTrafo = "CoordTrafo";

        private string fileName;
        public string FileName
        {
            get => fileName;
            
            set 
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException($"Name of {nameof(value)} must not be in empty in class {nameof(ExcelAccess)}");
                }
                fileName = value;
            }
        }

        public ExcelAccess(string fileName)
        {
           FileName = fileName;
        }

        public bool CreateNew(out string msg)
        {
            msg = string.Empty;
            Excel.Application xlApp = null;
            Excel.Workbook wkb = null;
            try
            {
                xlApp = new Excel.Application();
                wkb = xlApp.Workbooks.Add(Type.Missing);
                wkb.SaveAs(FileName);

                Excel.Worksheet newSheet;
                Excel.Range formatRange;

                List<string> sheetList = new List<string>(new string[] { sheetCmdLine, sheetCoordTrafo, sheetDAT_2_PTS, sheetPTS_2_DAT});
                foreach (var sheetName in sheetList)
                {
                    newSheet = wkb.Worksheets.Add(wkb.Worksheets[1],
                        Type.Missing, Type.Missing, Type.Missing);
                    newSheet.Name = sheetName;
                    formatRange = newSheet.get_Range("a1").EntireRow.EntireColumn;
                    formatRange.NumberFormat = "@";
                }
                wkb.Save();


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            finally
            {
                if (wkb != null)
                {
                    wkb.Save();
                    wkb.Close();
                    Marshal.ReleaseComObject(wkb);
                }
                if (xlApp != null)
                {
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlApp);
                }
            }
            return true;
        }

        [Obsolete]
        public Dictionary<string, List<CommandLine>> GetData()
        {
            var retVal = new Dictionary<string, List<CommandLine>>();
            Excel.Application xlApp = null;
            Excel.Workbook wkb = null;

            try
            {
                xlApp = new Excel.Application();
                wkb = xlApp.Workbooks.Open(FileName);
                Excel.Worksheet sheet= wkb.Sheets["Tabelle1"] as Excel.Worksheet;
                Excel.Range rangeKeys = null;
                Excel.Range rangeK = null;
                Excel.Range rangeTRAFO = null;
                Excel.Range rangeIN = null;
                Excel.Range rangeOUT = null;
                if (sheet != null)
                {
                    rangeKeys = sheet.UsedRange;
                    foreach (Excel.Range c in rangeKeys.Rows)
                    {
                        rangeK = c.Columns[1];
                        string s = rangeK.Value2;
                        rangeTRAFO = c.Columns[2];
                        string trafo = rangeTRAFO.Value2;
                        rangeIN = c.Columns[3];
                        string inp = rangeIN.Value2;
                        rangeOUT = c.Columns[4];
                        string outp = rangeOUT.Value2;


                        retVal.Add(s, new List<CommandLine> { new CommandLine(@"CMD.exe", new List<string> { "/C", trafo, inp, outp }) });
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                if (wkb != null)
                {
                    wkb.Close();
                    Marshal.ReleaseComObject(wkb);
                }
                if (xlApp != null)
                {
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlApp);
                }
            }
            return retVal;
        }

        public void WriteGeoTransCmd(long rowNum, string datFile, string trafo, string inpF, string outpF)
        {
            Excel.Application xlApp = null;
            Excel.Workbook wkb = null;

            try
            {
                xlApp = new Excel.Application();
                wkb = xlApp.Workbooks.Open(FileName);

                Excel.Worksheet sheet = (Excel.Worksheet)wkb.Sheets[sheetCmdLine];
                if (sheet == null)
                {
                    return;
                }

                Excel.Range columnA = sheet.Columns[1];
                Excel.Range columnB = sheet.Columns[2];
                Excel.Range columnC = sheet.Columns[3];
                Excel.Range columnD = sheet.Columns[4];
                Excel.Range keyCell = columnA.Rows[rowNum];
                keyCell.Value2 = datFile;
                Excel.Range trafoCell = columnB.Rows[rowNum];
                trafoCell.Value2 = trafo;
                Excel.Range inpdatCell = columnC.Rows[rowNum];
                inpdatCell.Value2 = "<" + "\"" + inpF + "\"";
                Excel.Range outpdatCell = columnD.Rows[rowNum];
                outpdatCell.Value2 = ">" + "\"" +  outpF + "\"";

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                if (wkb != null)
                {
                    wkb.Save();
                    wkb.Close();
                    Marshal.ReleaseComObject(wkb);
                }
                if (xlApp != null)
                {
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlApp);
                }
            }


        }

        public void Log(string sheetName, string keyValue, CellContentByColumn cellContentByColumn, params CellContentByColumn[] additionalContent)
        {
            Excel.Application xlApp = null;
            Excel.Workbook wkb = null;

            try
            {
                xlApp = new Excel.Application();
                wkb = xlApp.Workbooks.Open(FileName);

                Excel.Worksheet sheet = (Excel.Worksheet)wkb.Sheets["Tabelle1"];
                if (sheet == null)
                {
                    return;
                }

                Excel.Range columnA = sheet.UsedRange.Columns[1];
                foreach (Excel.Range cell in columnA.Cells)
                {
                    if (cell.Value2 != keyValue)
                    {
                        continue;
                    }

                    Excel.Worksheet logSheet = (Excel.Worksheet)wkb.Sheets[sheetName];
                    if (logSheet == null)
                    {
                        continue;
                    }

                    Excel.Range rangeWrite = logSheet.Cells[cell.Row, cellContentByColumn.Column];
                    rangeWrite.Value2 = cellContentByColumn.Content;
                    foreach (CellContentByColumn cC in additionalContent)
                    {
                        rangeWrite = logSheet.Cells[cell.Row, cC.Column];
                        rangeWrite.Value2 = cC.Content;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                if (wkb != null)
                {
                    wkb.Save();
                    wkb.Close();
                    Marshal.ReleaseComObject(wkb);
                }
                if (xlApp != null)
                {
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlApp);
                }
            }


        }

    }
}
