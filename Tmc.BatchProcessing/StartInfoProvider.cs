﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace Tmc.BatchProcessing
{
    public class StartInfoProvider
    {
        public static Dictionary<string, ProcessStartInfo> GetStartInfos(Dictionary<string, List<CommandLine>> processes)
        {
            var startInfos = new Dictionary<string, ProcessStartInfo>();
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, List<CommandLine>> entry in processes)
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                foreach (var cmdLine in entry.Value)
                {
                    startInfo.FileName = cmdLine.TaskName;
                    startInfo.WindowStyle = ProcessWindowStyle.Normal;
                    sb.Clear();
                    foreach (var cmdArg in cmdLine.CmdLineArgs)
                    {
                        sb.Append(cmdArg + " ");
                    }
                    startInfo.Arguments = sb.ToString();
                }
                startInfos.Add(entry.Key, startInfo);
            }
            return startInfos;
        }
    }
}
