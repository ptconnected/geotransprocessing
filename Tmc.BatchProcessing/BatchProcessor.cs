﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace Tmc.BatchProcessing
{

   

    class BatchProcessor
    {
       

        static void Main(string[] args)
        {
            if ((args.Length < 2) || (!Directory.Exists(args[0])) || (!args[1].StartsWith("gntrans")))
            {
                System.Console.WriteLine("INCORRECT USAGE!  FIRST -projDir SECOND : TRAFO   MAKE SURE TO USE DOUBLE QUOTES ON ARGUMENTS AS THEY CONTAIN SPACES (TRAFO AT ANY RATE)");
                return;
            }

            var dirProj = args[0];
            var trafo = args[1];

            bool deleteAllAtStart = ((args.Length >= 3) && (args[2] == "1"));
            bool deleteAllAtRun = ((args.Length >= 4) && (args[3] == "1"));
            
            bool excelLogging = ((args.Length >= 5) && (args[4] == "1")) && deleteAllAtStart;
            
            var tobeReplaced = string.Empty;
            var replacement = string.Empty;
            bool replace = (args.Length >= 7);
            if (replace)
            {
                tobeReplaced = args[5];
                replacement = args[6];
            }

            bool continueProcess = true;
            string msg;
            ExcelAccess excel = null;
            if (FileProcessor.CreateDirs(dirProj, out msg, deleteAllAtStart))
            {
                if (excelLogging)
                {
                    excel = new ExcelAccess(Path.Combine(dirProj, FileProcessor.XLSDir, ExcelAccess.xlsName));
                    if (!excel.CreateNew(out msg))
                    {
                        Console.WriteLine(msg);
                        excel = null;
                        continueProcess = false;
                    }
                }

                if (continueProcess)
                {
                    SplitFiles(dirProj); //raw => Inp
                    Dictionary<string, List<CommandLine>> cmdDic = new Dictionary<string, List<CommandLine>>();
                    RunPTS_2_DAT(excel, dirProj, trafo, cmdDic); //Inp =>InpDAT
                    RunGeoTransParallelOption(excel, dirProj, cmdDic); // RunGeoTrans(excel, dirProj, cmdDic);//InpDAT => OutpDAT
                    if (deleteAllAtRun)
                    {
                        try
                        {
                            DirectoryInfo dI = new DirectoryInfo(Path.Combine(dirProj, FileProcessor.InpDATDir));
                            dI.GetFiles().ToList().ForEach(fi => fi.Delete());
                        }
                        catch
                        {
                        }

                    }

                    RunDAT_2_PTS(excel, dirProj, trafo);//OutpDAT => Outp
                    if (deleteAllAtRun)
                    {
                        try
                        {
                            DirectoryInfo dI = new DirectoryInfo(Path.Combine(dirProj, FileProcessor.OutpDATDir));
                            dI.GetFiles().ToList().ForEach(fi => fi.Delete());
                        }
                        catch 
                        {
                               
                        }
                       
                    }
                    SpliceFiles(dirProj);//Inp, Outp => TransformedSplicedPTS
                    if (deleteAllAtRun)
                    {
                        try
                        {
                            DirectoryInfo dI = new DirectoryInfo(Path.Combine(dirProj, FileProcessor.OutpDir));
                            dI.GetFiles().ToList().ForEach(fi => fi.Delete());
                            dI = new DirectoryInfo(Path.Combine(dirProj, FileProcessor.InpDir));
                            dI.GetFiles().ToList().ForEach(fi => fi.Delete());
                        }
                        catch 
                        {
                           
                        }
                    }
                    if ((replace) && (!string.IsNullOrWhiteSpace(tobeReplaced))) //replacement can be empty
                    {
                       FileProcessor.RenamePTS(Path.Combine(dirProj, FileProcessor.TransformedSplicedPTSDir),
                            tobeReplaced, replacement);
                    }
                }
            }
            else
            {
                Console.WriteLine(msg);
            }

            Console.WriteLine("FINISHED. Press any key..");
            Console.ReadKey();
        }

        static void SpliceFiles(string dirProj)
        {
            
            DirectoryInfo dirOutp = new DirectoryInfo(Path.Combine(dirProj, FileProcessor.OutpDir));

            Dictionary<string, Dictionary<int,FileInfo>> splicerDic = new Dictionary<string, Dictionary<int, FileInfo>>();

            foreach (var fi in dirOutp.GetFiles("*.pts").OrderBy(fil => fil.Name))
            {
                
                if (fi.Name.EndsWith("_PART_SP.pts"))
                {
                    var temp = fi.Name.Replace("_PART_SP.pts", "");
                    var ind = temp.LastIndexOf('_');
                    var num = temp.Substring(ind + 1);
                    int ord = -1;
                    if (int.TryParse(num, out ord))
                    {
                        var fiOrigFileName = temp.Substring(0, ind);
                        if (!splicerDic.ContainsKey(fiOrigFileName))
                        {
                            var dic = new Dictionary<int, FileInfo>();
                            dic.Add(ord, fi);
                            splicerDic.Add(fiOrigFileName,dic);
                        }
                        else
                        {
                            splicerDic[fiOrigFileName].Add(ord, fi);
                        }
                    }
                }
                else
                {
                    //there were no
                    File.Copy(fi.FullName, Path.Combine(dirProj, FileProcessor.TransformedSplicedPTSDir, fi.Name));
                }
               

            }
            FileProcessor.SpliceFiles(dirProj, splicerDic);
        }

        static void SplitFiles(string dirProj)
        {
            DirectoryInfo dirDatRaw = new DirectoryInfo(Path.Combine(dirProj, FileProcessor.RawDir));
            foreach (var fi in dirDatRaw.GetFiles("*.pts"))
            {
                FileProcessor.SplitPTS(fi.FullName,  dirProj, fi.Name);
            }
        }


        static void RunDAT_2_PTS(ExcelAccess excel, string dirProj, string trafo)
        {
            DirectoryInfo dirDatIn = new DirectoryInfo(Path.Combine(dirProj, FileProcessor.OutpDATDir));
            DirectoryInfo dirPtstIn = new DirectoryInfo(Path.Combine(dirProj, FileProcessor.InpDir));
            foreach (var fi in dirDatIn.GetFiles("*.dat"))
            {
                string namePTSIn = fi.Name.Replace("_o.dat", ".pts");
                //it has to be unique..
                var filePTSInfo = dirPtstIn.GetFiles(namePTSIn).First();
                var namePTSOut = filePTSInfo.Name;
                var nameDatIn = namePTSOut.Replace(".pts", "_i.dat");
                excel?.Log(ExcelAccess.sheetDAT_2_PTS, nameDatIn, new CellContentByColumn(1, nameDatIn),
                    new CellContentByColumn[] { new CellContentByColumn(2, GetDateTimeString()) });
                var status = (FileProcessor.DAT_2_PTS(filePTSInfo.FullName, fi.FullName,
                    Path.Combine(dirProj, FileProcessor.OutpDir, namePTSOut)))
                    ? "OK"
                    : "ER";
                excel?.Log(ExcelAccess.sheetDAT_2_PTS, nameDatIn, new CellContentByColumn(3, GetDateTimeString()),
                    new CellContentByColumn[]
                    {
                        new CellContentByColumn(4, status),
                    });
            }
        }

        static void RunPTS_2_DAT(ExcelAccess excel, string dirProj, string trafo, 
            Dictionary<string, List<CommandLine>> cmdDic)
        {
            long rowNum = 0;
            
            DirectoryInfo dirIn = new DirectoryInfo(Path.Combine(dirProj, FileProcessor.InpDir));


            foreach (var fi in dirIn.GetFiles("*.pts"))
            {
                string nameDatIn = fi.Name.Replace(".pts", "_i.dat");
                string nameDatOut = fi.Name.Replace(".pts", "_o.dat");
                string fullNameIn = Path.Combine(dirProj, FileProcessor.InpDATDir, nameDatIn);
                string fullNameOut = Path.Combine(dirProj, FileProcessor.OutpDATDir, nameDatOut);
                rowNum++;
                excel?.WriteGeoTransCmd(rowNum,nameDatIn,trafo, fullNameIn, fullNameOut);
                cmdDic.Add(nameDatIn, new List<CommandLine> { new CommandLine(@"CMD.exe", 
                    new List<string> { "/C", trafo, 
                        "<" + "\"" + fullNameIn + "\"",
                        ">" + "\"" + fullNameOut + "\"" }) });

                excel?.Log(  ExcelAccess.sheetPTS_2_DAT, nameDatIn, new CellContentByColumn(1, nameDatIn),
                    new CellContentByColumn[] { new CellContentByColumn(2, GetDateTimeString()) });
                var status = (FileProcessor.PTS_2_DAT(fi.FullName, fullNameIn)) ? "OK" : "ER";
                excel?.Log(ExcelAccess.sheetPTS_2_DAT, nameDatIn, new CellContentByColumn(3, GetDateTimeString()),
                    new CellContentByColumn[]
                    {
                        new CellContentByColumn(4, status),
                    });

            }

        }
        [Obsolete]
        static void RunGeoTrans(ExcelAccess excel, string dirProj, Dictionary<string, List<CommandLine>> cmdDic)
        {
            
            var startInfos = StartInfoProvider.GetStartInfos(cmdDic);



            Parallel.For(0, startInfos.Count, i =>
           {
               var kvp = startInfos.ElementAt(i);
               string timestamp;
               try
               {
                   int? exitCode = null;
                   timestamp = GetDateTimeString();
                   excel?.Log(ExcelAccess.sheetCoordTrafo, kvp.Key, new CellContentByColumn(1, kvp.Key),
                       new CellContentByColumn[] { new CellContentByColumn(2, timestamp) });
                   using (Process exeProcess = Process.Start(kvp.Value))
                   {
                       exeProcess?.WaitForExit();
                       exitCode = exeProcess?.ExitCode;
                   }

                   var outLog = (exitCode == null) ? "NULL" : exitCode.ToString();
                   Console.WriteLine($"");
                   timestamp = GetDateTimeString();
                   excel?.Log(ExcelAccess.sheetCoordTrafo, kvp.Key, new CellContentByColumn(3, timestamp),
                       new CellContentByColumn[]
                       {
                            new CellContentByColumn(4, "OK"),
                            new CellContentByColumn(5, outLog)
                       });

               }
               catch
               {
                   timestamp = GetDateTimeString();
                   excel?.Log(ExcelAccess.sheetCoordTrafo, kvp.Key, new CellContentByColumn(3, timestamp),
                       new CellContentByColumn[] { new CellContentByColumn(4, "ER") });
               }
           });

        }


        static void RunGeoTransParallelOption(ExcelAccess excel, string dirProj, Dictionary<string, List<CommandLine>> cmdDic)
        {

            var startInfos = StartInfoProvider.GetStartInfos(cmdDic);



            var procCount    = System.Environment.ProcessorCount;
            Parallel.ForEach(startInfos,
                new ParallelOptions { MaxDegreeOfParallelism = procCount}, kvp =>
            {
                
                string timestamp;
                try
                {
                    int? exitCode = null;
                    timestamp = GetDateTimeString();
                    excel?.Log(ExcelAccess.sheetCoordTrafo, kvp.Key, new CellContentByColumn(1, kvp.Key),
                        new CellContentByColumn[] { new CellContentByColumn(2, timestamp) });
                    using (Process exeProcess = Process.Start(kvp.Value))
                    {
                        exeProcess?.WaitForExit();
                        exitCode = exeProcess?.ExitCode;
                    }

                    var outLog = (exitCode == null) ? "NULL" : exitCode.ToString();
                    Console.WriteLine($"");
                    timestamp = GetDateTimeString();
                    excel?.Log(ExcelAccess.sheetCoordTrafo, kvp.Key, new CellContentByColumn(3, timestamp),
                        new CellContentByColumn[]
                        {
                            new CellContentByColumn(4, "OK"),
                            new CellContentByColumn(5, outLog)
                        });

                }
                catch
                {
                    timestamp = GetDateTimeString();
                    excel?.Log(ExcelAccess.sheetCoordTrafo, kvp.Key, new CellContentByColumn(3, timestamp),
                        new CellContentByColumn[] { new CellContentByColumn(4, "ER") });
                }
            });


        }




        private static string GetDateTimeString()
        {
            var dt = DateTime.Now;
            var s = $"{dt:yyyy - MM - dd HH: mm: ss.ff}";
            return s;
        }
    }
}
