﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Office.Interop.Excel;

namespace Tmc.BatchProcessing
{
    public static class FileProcessor
    {
        public const string RawDir = "Raw";
        public const string InpDir = "Inp";
        public const string InpDATDir = "InpDAT";
        public const string OutpDATDir = "OutpDAT";
        public const string OutpDir = "Outp";
        public const string TransformedSplicedPTSDir = "TransformedSplicedPTS";
        public const string XLSDir = "XLS";
         
        public static bool CreateDirs(string projDir, out string msg, bool delete = false)
        {
            msg = string.Empty;
            try
            {
                var dirTemp = Path.Combine(projDir, RawDir);
                if (Directory.Exists(dirTemp))
                {

                    List<string> dirList = new List<string>(new string[] {InpDATDir, InpDir, OutpDATDir, OutpDir, TransformedSplicedPTSDir , XLSDir});
                    foreach (var dirConst in dirList)
                    {
                        dirTemp = Path.Combine(projDir, dirConst);
                        if (Directory.Exists(dirTemp))
                        {
                            if (delete)
                            {
                                try
                                {
                                    Directory.Delete(dirTemp, true);
                                }
                                catch (Exception e)
                                {
                                    msg = $"Directory \"{dirTemp}\" could not be deleted! Exception";
                                    msg += e.ToString();
                                    return false;
                                }
                            }
                            else
                            {
                                msg = $"Directory \"{dirTemp}\" exists already! Clear manually";
                                return false;
                            }
                        }
                        Directory.CreateDirectory(dirTemp);
                        if (!Directory.Exists(dirTemp))
                        {
                            msg = $"Directory \"{dirTemp}\" could not be created!";
                            return false;
                        }
                    }
                }
                else
                {
                    msg = $"Directory \"{dirTemp}\" does not exist!";
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

        public static bool RenamePTS(string renameDir, string toBeReplaced, string replacement)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(renameDir);
            try
            {
                foreach (var fI in dirInfo.GetFiles("*.pts"))
                {

                    var oldName = fI.Name.Remove(fI.Name.Length - ".pts".Length);
                    int ind = -1;
                    //in name not in Path to Name! This would be FullName
                    if ((ind = oldName.LastIndexOf(toBeReplaced)) != -1)
                    {
                        var newName = oldName.Remove(ind, toBeReplaced.Length).Insert(ind, replacement) + ".pts";
                        var fullNewName = Path.Combine(renameDir, newName);
                        fI.MoveTo(fullNewName);

                    }
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }
        public static bool PTS_2_DAT(string fileIn, string fileOut)
        {
            long i = 0;
            using (var fileSr = new StreamReader(fileIn))
            using (var fileSw = new StreamWriter(fileOut))
            {
                try
                {
                    fileSw.AutoFlush = true;
                    string line = fileSr.ReadLine();
                    //line contains now number of points in pts file
                    var sb = new StringBuilder();
                    string[] subs = new string[7];
                    while ((line = fileSr.ReadLine()) != null)
                    {
                        sb.Clear();
                        i++;
                        subs = line.Split(' ');
                        sb.Append('P' + i.ToString());
                        sb.Append(' ');
                        sb.Append(subs[0]);
                        sb.Append(' ');
                        sb.Append(subs[1]);
                        sb.Append(' ');
                        sb.Append(subs[2]);
                        fileSw.WriteLine(sb.ToString());
                    }
                }
                catch (Exception e)
                {
                    //Excel report...
                    Console.WriteLine($"PTS_2_DAT ERROR FILE : {fileIn}");
                    Console.WriteLine(e);
                    return false;

                }
                finally
                {
                    fileSr.Close();
                    fileSw.Close();
                }

            }

            Console.WriteLine($"PTS_2_DAT SUCCESS FILE : {fileIn}");
            return true;
        }

        public static bool SpliceFiles(string projDir, Dictionary<string, Dictionary<int, FileInfo>> splicerDic)
        {

            foreach (var f in splicerDic.Keys)
            {
                long total = 0;
                foreach (var kvp in splicerDic[f].OrderBy(n => n.Key))
                {
                    using (StreamReader sr = new StreamReader(kvp.Value.FullName))
                    {
                        long temp = 0;
                        var inLine = sr.ReadLine();
                        if (long.TryParse(inLine, out temp))
                        {
                            total += temp;
                        }

                    }

                }


                var fileName = Path.Combine(projDir, "TransformedSplicedPTS", f + ".pts");
                using (StreamWriter sw = new StreamWriter(fileName))
                {
                    sw.WriteLine(total.ToString());
                    foreach (var kvp in splicerDic[f].OrderBy(n => n.Key))
                    {
                        using (StreamReader sr = new StreamReader(kvp.Value.FullName))
                        {
                            //First line ccontains number of points
                            var inLine = sr.ReadLine();
                            while ((inLine = sr.ReadLine()) != null)
                            {
                                sw.WriteLine(inLine);
                            }

                        }

                    }
                    sw.Flush();
                    sw.Close();
                }
            }
            return true;
        }

        public static bool SplitPTS(string fileIn,  string projDir, string nameFi)
        {
            const long MaxSize = 5000000; //50000000; //5;
            try
            {
                int numberFilesWritten = 0;
                using (var fileSr = new StreamReader(fileIn))
                {
                    long totCount = -1;
                    long j = 0;
                    long k = 0;
                   
                    var lineIn = fileSr.ReadLine();
                    if ((long.TryParse(lineIn, out totCount)) && (totCount > 0))
                    {
                        if (totCount <= MaxSize)
                        {
                            fileSr.Close();
                            File.Copy(fileIn, Path.Combine(projDir, "Inp", nameFi));
                            return true;
                        }
                        else
                        {
                            int fileCounts = (int)(totCount / MaxSize + 1);
                            var residue = totCount % MaxSize;
                            if (residue == 0)
                                fileCounts--;

                            var fileName = fileIn.Replace(".pts", "_SPLIT_XYZ.pts");
                            var fileWr = new StreamWriter(fileName);

                            fileWr.WriteLine(MaxSize.ToString());
                            lineIn = fileSr.ReadLine();
                            while (lineIn != null)
                            {
                                j++;
                                k++;
                                if ((k <= MaxSize) && (j <= totCount))          //(((k <= MaxSize) && (numberFilesWritten < fileCounts - 1)) || ((numberFilesWritten < fileCounts - 1) && (k <= residue)))
                                {
                                    fileWr.WriteLine(lineIn);
                                    lineIn = fileSr.ReadLine();

                                }
                                else
                                {
                                    k = 0;
                                    j--;
                                    numberFilesWritten++;
                                    fileWr.Flush();
                                    fileWr.Close();
                                    File.Move(fileName, 
                                        Path.Combine(projDir, "Inp", nameFi.Replace(".pts", "_" + numberFilesWritten.ToString() +"_PART_SP.pts")));
                                    fileWr.Dispose();
                                    
                                    if (numberFilesWritten  < fileCounts)
                                    {
                                        
                                        fileWr = new StreamWriter(fileName);
                                        if (numberFilesWritten == (fileCounts - 1))
                                        {
                                            if (residue > 0)
                                                fileWr.WriteLine(residue.ToString());
                                            else
                                                fileWr.WriteLine(MaxSize.ToString());
                                        }
                                        else
                                        {
                                            fileWr.WriteLine(MaxSize.ToString());
                                        }
                                        
                                    }
                                    
                                }

                            }

                            //if (residue > 0)
                            //{
                            fileWr.Flush();
                            fileWr.Close();
                            numberFilesWritten++;
                            File.Move(fileName,
                                Path.Combine(projDir, "Inp", nameFi.Replace(".pts", "_" + numberFilesWritten.ToString() + "_PART_SP.pts")));

                            //}
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;

        }



        public static bool DAT_2_PTS(string filePTSIn, string fileDATIn, string fileOut)
        {
            bool checkErrorOnLine(string lineIn)
            {
                const string err = @"(s5)Protection check error! SEC_D not ready. Giving up.";
                bool retVal = false;
                retVal = (lineIn == err);
                return retVal;
             
            }

            using (var fileSrPTS = new StreamReader(filePTSIn))
            using (var fileSrDAT = new StreamReader(fileDATIn))
            using (var fileSw = new StreamWriter(fileOut))
            {
                try
                {
                    fileSw.AutoFlush = true;
                    var sb = new StringBuilder();
                    string[] subsPTS = new string[7];
                    string[] subsDAT = new string[4];
                    //number of points...
                    var inLinePTS = fileSrPTS.ReadLine(); //first line contains number of points in pts
                    string inLineDAT;
                    fileSw.WriteLine(inLinePTS);

                    inLineDAT = fileSrDAT.ReadLine();
                    //dat file ...(s5)Protection check error! SEC_D not ready. Giving up.  written by geotrans on some occasions..
                    //we ignore those lines
                    while(checkErrorOnLine(inLineDAT))
                    {
                        inLineDAT = fileSrDAT.ReadLine();
                    }

                    while (((inLinePTS = fileSrPTS.ReadLine()) != null) && (inLineDAT != null))
                    {
                        sb.Clear();
                        subsPTS = inLinePTS.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        //for some reason or another gntrans introduces a lot of spaces into export file. We have to get rid of them with the stringspltoptions.
                        subsDAT = inLineDAT.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                        //first coordinate x- ignore Point-Label - would be at pos 0
                        sb.Append(subsDAT[1]);
                        sb.Append(' ');
                        //Appemd remaining co-ordinates from dat
                        sb.Append(subsDAT[2]);
                        sb.Append(' ');
                        sb.Append(subsDAT[3]);
                        sb.Append(' ');
                        //then take grey scale and rgb-values from pts..ignoring co-ordinates at pos 0, 1, 2
                        sb.Append(subsPTS[3]);
                        sb.Append(' ');
                        //there are cases where a pts-file doesn't have color information - only grey scale
                        if (subsPTS.Length >= 7)
                        {
                            sb.Append(subsPTS[4]);
                            sb.Append(' ');
                            sb.Append(subsPTS[5]);
                            sb.Append(' ');
                            sb.Append(subsPTS[6]);
                            
                        }
                        fileSw.WriteLine(sb.ToString());
                        inLineDAT = fileSrDAT.ReadLine();
                        //make sure..also in interior of File .. although error has never occured inside file before but only at the very beginningh of a file .. interestingly for the first 20 line
                        while (checkErrorOnLine(inLineDAT))
                        {
                            inLineDAT = fileSrDAT.ReadLine();
                        }

                    }

                    if ((!fileSrPTS.EndOfStream) || (!fileSrDAT.EndOfStream))
                    {
                        //have to contain same number of points...
                        Console.WriteLine($"DAT_2_PTS ERROR FILE : {fileDATIn}");
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.WriteLine($"DAT_2_PTS ERROR FILE : {fileDATIn}");
                    return false;
                }
                finally
                {
                    fileSrPTS.Close();
                    fileSrDAT.Close();
                    fileSw.Close();
                }
            }
            Console.WriteLine($"DAT_2_PTS SUCCESS FILE : {fileDATIn}");
            return true;
        }
    }
}
