﻿using System.Collections.Generic;
using System.IO;
using System.Security.Policy;
using System;


namespace Tmc.BatchProcessing
{
    [Obsolete]
    class ProcessProvider
    {
        //The idea is to read the information about the tasks to be processed from an excel file (or similar)
        //and write the log back to the same source - like starting time, finishing time possible status etc

        public static Dictionary<string, List<CommandLine>> GetProcesses(ExcelAccess excelAcc)
        {
            var processes = excelAcc.GetData();
            return processes;
        }

    }
}
